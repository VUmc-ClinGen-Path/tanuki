# Changelog

## [2.0.0](https://gitlab.com/hg-aumc/genome-analysis/tanuki/compare/v1.6.0...v2.0.0) (2025-02-05)


### ⚠ BREAKING CHANGES

* Allowing alignement of several files per sample corresponding at different lanes. Fixes #21

### Features

* Allowing alignement of several files per sample corresponding at different lanes. Fixes [#21](https://gitlab.com/hg-aumc/genome-analysis/tanuki/issues/21) ([f124f2c](https://gitlab.com/hg-aumc/genome-analysis/tanuki/commit/f124f2cdff67d383a4727f9cea585b2b3f851c47))

## [1.6.0](https://gitlab.com/hg-aumc/genome-analysis/tanuki/compare/v1.5.0...v1.6.0) (2024-04-23)


### Features

* Adding files or auto release according to BH64 ([7328672](https://gitlab.com/hg-aumc/genome-analysis/tanuki/commit/7328672491bf9b6631bb28920c805b549b1e6e4f))
