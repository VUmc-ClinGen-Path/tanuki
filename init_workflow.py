#!/bin/python3

"""
A very friendly (too friendly ?) script that creates a config file to run Tanuki for you.
Option to make it prepare jasmine tea is under development.
"""

import os
import argparse
import yaml
from pathlib import Path
import re

base = Path(__file__).parent
configfile = base.joinpath("config/config.yaml")
f = open(configfile,"r")
config = yaml.load(stream=f,Loader=yaml.SafeLoader)
f.close()


def get_arguments():
  """Parsing the arguments"""
  parser = argparse.ArgumentParser(description="Creates a proper config file to start the snakemake pipeline : Tanuki",
  usage='''
______________________________________________________________________
                       ***** Tanuki *****
                 A pipeline for RNAseq analysis
______________________________________________________________________
Generic command: python init_workflow.py [Options]* 
Arguments:
    -t, --test                 : Test the pipeline using test dataset.
    -i, --indir  INDIR         : Input directory containing all reads to be analyzed in fastq.gz format
    -o, --outdir OUTDIR        : Output directory OTUDIR where to store all the results
    -r, --reference            : (Default : human) Which model organism to pick for alignement. Chose from human or mouse.
    --unpaired                 : (Default : False). If your input reads are unpaired
    --export-qc                : To export the Multiqc report into a folder on M drive to be seen by analyst
    ______________________________________________________________________
''')
  parser.add_argument("-t", "--test", help="Test the pipeline using test dataset.", required=False, action='store_true')
  parser.add_argument("-i", "--indir", help="Provide a full path of your input directory containing all reads to be analyzed", default="None")
  parser.add_argument("-o", "--outdir", help="Output directory where to store all the results", default="None")
  parser.add_argument("-r","--reference",help="(Default : human) Which model organism to pick for alignement. Chose from human or mouse.",default="human")  
  parser.add_argument("--unpaired", help="(Default : False. Which extension are you using for paired reads", action='store_true', default=False)
  parser.add_argument("--export-qc",help="To export the Multiqc report into a folder on M drive to be seen by analyst",action='store_true',default="False")
  return (parser.parse_args())

def confirm_overwrite():
  """
  Ask user to enter Y or N (case insensitive)
  Return True if answer is Yes
  Will ask for confirmation before overwriting existing config file
  """
  answer = ""
  while answer not in ["y","n"]:
    answer = input("It seems a config file for tanuki already exists. Do you want to overwrite it [Y/N] ? : ").lower()
  return answer == "y"

def check_reference(reference):
  if reference == "human" or reference == "mouse":
    return reference
  else:
    print("You provided a reference that is unknown ! I only know of human or mouse. Please pick one of the two or add a new one as a feature.")
    exit()

def create_samples_list(indir):
  #Recognize both paired and unpaired reads
  sample_pattern = r'(?P<sample>.+)_(?P<snumb>.+)_(?P<lane>L00[0-9])(?P<read>_R[1-2])?_001\.f.*q(\.gz)?'
  sample_list = []
  files = os.listdir(indir)
  try:
    #Check all fastq files and their names to detect samples and add them to a list of sample
    for file in files:
      match = re.match(sample_pattern,file)
      if match:
        sample = match.group('sample')        
        if sample not in sample_list:
          sample_list.append(sample)              
    if(len(sample_list) == 0):
      print("No samples could be recognized !")
      print(f"Make sure that samples in this path : {indir}")
      print(f"Fit the following pattern : {sample_pattern}")
      print(f"Files found : {sample_list}")
      print("Please fix the names of files to fit this pattern.")
      exit()
  except Exception as e:
    print(f"It seems something unexpected happened while building the list of samples ! See details below :")
    print(e)
    raise(e)
  return sample_list

def get_project_name(indir):
  project_name = indir.split("/")[0]
  return project_name

def get_pool_name(indir):
  pool_name = indir.split("/")[2]
  return pool_name

def get_flowcell(indir):
  flowcell = indir.split("/")[3]
  return flowcell

def main():
    args = get_arguments()
    workflow = {}

    #Change which config file to load if test mode or not
    if args.test:
      if args.unpaired == False:
        mode="paired"
      else:
        mode="unpaired"
      workflow["indir"] = config["test_dataset"][mode]
      workflow["outdir"] = config["test_dataset"]["output"]
      workflow["unpaired"] = args.unpaired
      workflow["reference"] = "human"
      workflow["l_qc_export_path"] = config["l_qc_export_path"]
      workflow["m_qc_export_path"] = config["m_qc_export_path"]
      workflow["runid"] = "TEST_poolID_flowcellID"

    elif not args.test and ( args.indir == "None" or args.outdir == "None"):
      print("Hey ! ( •̀_•́) Haven't you forgotten to give the required options -i/--indir AND -o/--outdir ? I can't work without them.")
      exit()
    else:
      #Defining all parameters to write into generated config file
      workflow["indir"] = args.indir
      workflow["outdir"] = args.outdir
      workflow["unpaired"] = args.unpaired
      workflow["reference"] = check_reference(args.reference)
      if args.export_qc:
        #Substract project folder directory path to input path
        #extracted_path example : PROJECTID/data/POOLID/FLOWCELLID
        #/!\NEED TO TEST HERE EXCEPTION
        extracted_path = os.path.relpath(workflow["indir"], config["project_dir_path"])
        print(f"dir : {extracted_path}")
        #Create new qc_export_path
        workflow["l_qc_export_path"] = config["l_qc_export_path"]
        workflow["m_qc_export_path"] = config["m_qc_export_path"]
        runid = get_project_name(extracted_path)
        runid += "_" + get_pool_name(extracted_path)
        runid += "_" + get_flowcell(extracted_path)
        workflow["runid"] = runid
      else:
        workflow["l_qc_export_path"] = ".nan"
        workflow["m_qc_export_path"] = ".nan"

    #Set all the path of references file
    workflow["transcripts"] = config[args.reference]["transcripts"]
    workflow["star_index"] = config[args.reference]["star_index"]
    workflow["bowtie_index"] = config[args.reference]["bowtie_index"]
    workflow["gtf_transcripts"] = config[args.reference]["gtf_transcripts"]

    #Fill in sample list
    workflow["samples"] = create_samples_list(workflow["indir"])

    
    
    #Checking if outdir path exists
    outdir_path = Path(workflow["outdir"])
    isExist = os.path.exists(outdir_path)
    if not isExist:
      os.makedirs(outdir_path)

    #creating out config file to start snakemake pipeline with
    outfile = outdir_path.joinpath("config.yaml")

    #Checking if previous  config file already exists
    if outfile.is_file():
      if confirm_overwrite():
        pass
      else:
        print("Alright then, mission aborted (╥﹏╥). You can re-run me later when your config file is safe.")
        exit()
    #Write the new generated config file
    with open(outfile, "w") as f:
      yaml.dump(data=workflow, stream=f, sort_keys=False)
    run_command = f"snakemake --profile profile/ -j50 --configfile {outfile}"
    print(f"Config file created ! (｡◕‿‿◕｡)")
    print(f"You can run Tanuki using the following command : ")
    print(f"{run_command}")
    
if __name__ == "__main__":
  main()



