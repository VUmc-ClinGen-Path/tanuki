##############################################
#                   __        .-.
#               .-"` .`'.    /\\|
#       _(\-/)_" ,  .   ,\  /\\\/
#      {(#b^d#)} .   ./,  |/\\\/
#      `-.(Y).-`  ,  |  , |\.-`
#           /~/,_/~~~\,__.-`
#          ////~    // ~\\
#  jgs   ==`==`   ==`   ==`
#___________                    __   .__ 
#\__    ___/____    ____  __ __|  | _|__|
#  |    |  \__  \  /    \|  |  \  |/ /  |
#  |    |   / __ \|   |  \  |  /    <|  |
#  |____|  (____  /___|  /____/|__|_ \__|
#               \/     \/           \/ 
##############################################
######## PIPELINE FOR RNASEQ ANALYSIS ########
##############################################
#----------------------------------------------
#Author: Roxane Boyer
#Affiliation: Amsterdam UMC
#Department: Human Genetics - Bioinformatics
#Created: 30/01/2023
#----------------------------------------------

from pathlib import Path
import glob
import ruamel.yaml

#Setting how to export yaml
yaml = ruamel.yaml.YAML()
yaml.preserve_quotes = True

#Functions for slack hooks
include: "rules/functions.py"

# Workflow base directory
workflow_dir = Path(workflow.basedir).parent
configfile: workflow_dir.joinpath("config/config.yaml")
slack_hooks_file = config["slack_hooks_file"]

indir = config["indir"]
outdir = config["outdir"]
log_dir = outdir + "/logs"

l_qc_export_path = config["l_qc_export_path"]
m_qc_export_path = config["m_qc_export_path"]
mail_notification = config["mail_notification"]

unpaired = config["unpaired"]
SAMPLES = config["samples"]

#Set export QC mode
if l_qc_export_path == ".nan":
    qc_export=False
    runid = "no_run_id"
else:
    qc_export=True
    runid = config["runid"]

#Lil trick to get a list of fastq files within snakemake for a certain sample
fastq_files = [f for f in os.listdir(indir) if f.endswith('.fastq.gz')]
def get_sample_fastq(sample):
    file_list = []
    for f in fastq_files:
        if f.startswith(sample+"_"):
            file_list.append(indir + "/" + f)
    return file_list

R1fastq_files = [f for f in os.listdir(indir) if re.match('.*R1.*\.fastq\.gz', f)]
def get_R1_fastq(sample):
    r1_list = [indir + "/" + f for f in R1fastq_files if f.startswith(sample+"_")]
    r1_list_sorted = sorted(r1_list)
    return r1_list_sorted

R2fastq_files = [f for f in os.listdir(indir) if re.match('.*R2.*\.fastq\.gz', f)]
def get_R2_fastq(sample):
    r2_list = [indir + "/" + f for f in R2fastq_files if f.startswith(sample+"_")]
    r2_list_sorted = sorted(r2_list)
    return r2_list_sorted



#Rule all slightly change depending on qc export path or not
if qc_export:
    rule all:
        input:
            quant = expand(outdir+"/salmon/{sample}/quant.sf", sample=SAMPLES),
            l_export_report = l_qc_export_path + "/" + runid + "/multiqc_report.html",
            m_export_report = m_qc_export_path + "/" + runid + "/multiqc_report.html",
else:
    rule all:
        input:
            quant = expand(outdir+"/salmon/{sample}/quant.sf", sample=SAMPLES),
            report = outdir + "/multiqc/multiqc_report.html"



rule star:
    input:
        r1_fastqs = lambda wildcards: get_R1_fastq(wildcards.sample),
        r2_fastqs = lambda wildcards: get_R2_fastq(wildcards.sample),
    output:
        #The name with no underscore before the word "Aligned" is triggering but necessary
        #As it is defined by STAR itself. Do not modify !
        bam = outdir + "/star/{sample}/{sample}Aligned.toTranscriptome.out.bam",
        log = outdir + "/star/{sample}/{sample}Log.final.out"
    params:
        outdir = outdir + "/star/{sample}",
        prefix = "{sample}",
        index = config["star_index"],
        log = log_dir,
        r1_files = lambda wildcards: ','.join(get_R1_fastq(wildcards.sample)),
        r2_files = lambda wildcards: ','.join(get_R2_fastq(wildcards.sample)),
    threads: config["cpu"]["star"]
    resources:
        mem_mb = config["memory"]["star"]
    conda:
        workflow_dir.joinpath("workflow/envs/tanuki-env.yaml")
    shell: """
        echo "Fastq for r1: {params.r1_files}"
        echo "Fastq for r2: {params.r2_files}"
        STAR --genomeDir {params.index} \
        --runThreadN {threads} \
        --readFilesIn {params.r1_files} {params.r2_files} \
        --readFilesCommand zcat \
        --outFileNamePrefix {params.outdir}/{params.prefix} \
        --outSAMtype None \
        --quantMode TranscriptomeSAM \
        """

rule salmon:
    input:
        bam = outdir + "/star/{sample}/{sample}Aligned.toTranscriptome.out.bam"
    output:
        quant = outdir + "/salmon/{sample}/quant.sf",
        meta = outdir + "/salmon/{sample}/aux_info/meta_info.json"
    params:
        outdir = outdir + "/salmon/{sample}",
        transcripts = config["transcripts"],
        log = log_dir
    threads: config["cpu"]["salmon"]
    resources:
        mem_mb = config["memory"]["salmon"]
    conda:
        workflow_dir.joinpath("workflow/envs/tanuki-env.yaml")
    shell: """
        salmon quant -l A -t {params.transcripts} -a {input.bam} -o {params.outdir};
        """

#Rule bowtie2 is Ribodepletion check
#We do this only on the first set of samples as a QC test, no need to do it for everything
if unpaired:
    rule bowtie2:
        input:
            fastqs = lambda wildcards: get_sample_fastq(wildcards.sample),
        output:
            log = outdir + "/bowtie2/{sample}_bowtie2.log"
        params:
            index = config["bowtie_index"],
            log = log_dir
        threads: config["cpu"]["bowtie2"]
        resources:
            mem_mb = config["memory"]["bowtie2"]
        conda:
            workflow_dir.joinpath("workflow/envs/tanuki-env.yaml")
        shell: """
            bowtie2 -x {params.index} -U {input.fastq} > /dev/null 2> {output.log} 
        """
else:
    rule bowtie2:
        input:
            r1 = lambda wildcards: get_R1_fastq(wildcards.sample)[0],
            r2 = lambda wildcards: get_R2_fastq(wildcards.sample)[0],
        output:
            log = outdir + "/bowtie2/{sample}_bowtie2.log"
        params:
            index = config["bowtie_index"],
            log = log_dir
        threads: config["cpu"]["bowtie2"]
        resources:
            mem_mb = config["memory"]["bowtie2"]
        conda:
            workflow_dir.joinpath("workflow/envs/tanuki-env.yaml")
        shell:
            """
                echo "Fastq for r1: {input.r1}"
                echo "Fastq for r2: {input.r2}"
                bowtie2 -x {params.index} -1 {input.r1} -2 {input.r2} > /dev/null 2> {output.log} 
            """
    

if unpaired:
    rule multiqc:
        input:
            bowtie_reports = expand(outdir + "/bowtie2/{sample}_bowtie2.log", sample=SAMPLES),
            salmon_outputs = expand(outdir + "/salmon/{sample}/aux_info/meta_info.json", sample=SAMPLES),
            star_outputs = expand(outdir + "/star/{sample}/{sample}Log.final.out", sample=SAMPLES),
        output:
            report = outdir + "/multiqc/multiqc_report.html"
        params:
            outdir = outdir + "/multiqc",
            bowtie_dir = outdir + "/bowtie2",
            salmon_dir = outdir + "/salmon",
            star_dir = outdir + "/star",
            log = log_dir
        resources:
            mem_mb = config["memory"]["multiqc"]
        conda:
            workflow_dir.joinpath("workflow/envs/tanuki-env.yaml")
        shell: """
            mkdir -p {params.outdir};
            multiqc --interactive {params.bowtie_dir} {params.salmon_dir} {params.star_dir} -o {params.outdir};
        """
else:
    rule multiqc:
        input:
            bowtie_reports = expand(outdir + "/bowtie2/{sample}_bowtie2.log", sample=SAMPLES),
            salmon_outputs = expand(outdir + "/salmon/{sample}/aux_info/meta_info.json", sample=SAMPLES),
            star_outputs = expand(outdir + "/star/{sample}/{sample}Log.final.out", sample=SAMPLES),
        output:
            report = outdir + "/multiqc/multiqc_report.html"
        params:
            outdir = outdir + "/multiqc",
            bowtie_dir = outdir + "/bowtie2",
            salmon_dir = outdir + "/salmon",
            star_dir = outdir + "/star",
            log = log_dir
        resources:
            mem_mb = config["memory"]["multiqc"]
        conda:
            workflow_dir.joinpath("workflow/envs/tanuki-env.yaml")
        shell: """
            mkdir -p {params.outdir};
            multiqc --interactive {params.bowtie_dir} {params.salmon_dir} {params.star_dir} -o {params.outdir};
        """
   

if qc_export:
    rule export_qc_report:
        input:
            report = outdir + "/multiqc/multiqc_report.html"
        output:
            l_export_report = l_qc_export_path + "/" + runid + "/multiqc_report.html",
            m_export_report = m_qc_export_path + "/" + runid + "/multiqc_report.html",
        params:
            mail = mail_notification,
            runid = runid
        localrule: True
        shell: """
            echo "Copying report {input.report} to L drive ( {output.l_export_report} ) : "
            cp {input.report} {output.l_export_report}
            echo "Copying report {input.report} to M drive ( {output.m_export_report} ) : "
            cp {input.report} {output.m_export_report}
            echo "Sending notification to lab that a  analysis MultiQC is ready."
            echo -e "Dear friends, \n\n A new MultiQC report is available on the L drive ({output.l_export_report} and on the M drive ({output.m_export_report}). Please have a look and release the data for it to be shared to our clients !\n\nA kind automatic script." | mail -s "[MultiQC] [RNAseq] {params.runid}" {params.mail}
        """

onerror:
    # Notify Bioinf-Klingen that pipeline failed
    error_emoji = ":blobsweat:"
    message = rf" :tanuki_pipeline: Tanuki pipeline FAILED for run {runid} {error_emoji} "
    slack_notification(message=message)


onsuccess:
    # Notify Bioinf-Klingen that pipeline succeeded
    success_emoji = ":pat:"
    message = rf" :tanuki_pipeline: Tanuki pipeline SUCCESSFULL for run {runid} {success_emoji}"
    slack_notification(message=message)
