def slack_notification(message, **kwargs):
    import subprocess

    slack_config = yaml.load(Path(slack_hooks_file))
    slack_hook = slack_config.get("SLACK_HOOK")

    if not slack_hook:
        print("Please define slack hook to get notifications on slack!")
        return

    subprocess.run(
        rf"""
        curl -X POST -H 'Content-type: application/json' --data '{{"text":"{message}"}}' {slack_hook}
    """,
        shell=True,
    )
