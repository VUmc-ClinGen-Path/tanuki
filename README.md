<p align="center"><img src="misc/tanuki_updated_banner.png" alt="Tanuki"></p>

# Tanuki 

Simple snakemake pipeline for RNAseq data pre-processing on AUMC Core Facility Genomic's cluster.

## Table of contents

* [Overview of the pipeline](#overview-of-the-pipeline)
* [Installation](#installation)
* [Usage (short)](#usage-i-dont-have-time-to-read)
* [Usage (detailed)](#usage-detailed-version)
* [Options](#options)
* [Output details](#output-files)
* [Logs](#logs)
* [Ressources and runnning time]()


## Overview of the pipeline

```plantuml
!define ICONURL https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/v2.4.0
!include ICONURL/common.puml
!include ICONURL/font-awesome-5/gitlab.puml
!include ICONURL/font-awesome-5/dna.puml
!include ICONURL/font-awesome-5/rocket.puml
!include ICONURL/font-awesome-5/leaf.puml
!include ICONURL/font-awesome-5/star.puml
!include ICONURL/font-awesome-5/fish.puml
!include ICONURL/font-awesome-5/search.puml
!include ICONURL/font-awesome-5/scroll.puml
left to right direction
FA5_DNA(dna,fastq files,node) #White    
FA5_STAR(star,alignement,node) #White
FA5_FISH(salmon,quantification,node) #White
FA5_SEARCH(bowtie,ribodepletion check,node) #White
FA5_SEARCH(fastqc,quality control,node) #White
FA5_SCROLL(multiqc,multiqc,node) #White

dna --> star
star --> salmon
salmon --> multiqc
dna --> bowtie
dna --> fastqc
fastqc --> multiqc
bowtie --> multiqc
```

Tanuki takes in input your raw reads from your RNAseq experiment and will perform the following steps : 
- General QC : FastqC
- Ribodepletion check : bowtie2 against 28S rRNA sequence (https://www.ncbi.nlm.nih.gov/nuccore/M11167.1/) 
- Alignment : STAR against GRCh38
- Quantification : salmon
- General report of all the steps above : multiQC

## Installation

To install tanuki, you can simply clone its repository from Gitlab in your own scratch folder : 

```bash
git clone git@gitlab.com:VUmc-ClinGen-Path/tanuki.git
```


## Usage (I don't have time to read)

In a rush  You should be able to run those easy 4 steps.

```bash
module load snakemake
python init_workflow.py -i "/path/to/my/raw/reads" -o "/path/to/store/my/results"
snakemake --profile profile/ -j50 --configfile config.yaml
```
Something didn't quite worked out or you want to understand more what each steps ? The section below is for you.

## Usage (detailed version)

To run, tanuki needs 2 module to be loaded : snakemake (as well it is a snakemake pipeline) and miniconda (so it can access to its proper environement to run)

```bash
module load snakemake
```

Tanuki has been developped to run on the CFG cluster and will take care of finding the reference and index it needs. Those references are indicated to tanuki via a yaml configuration file.

To create a proper configuration file for tanuki, you must first run the python script init_workflow.py in the following way : 

```bash
python init_workflow.py -i "/path/to/my/raw/reads" -o "/path/to/store/my/results"
```

If you wish to test tanuki with test data to verify it works on your system, you can do the following : 

```bash
python init_workflow.py -t
```

This will result in creating a config file looking like this :

```bash
#config.yaml
indir: .test/data/reads
outdir: .test/results
extension: .fastq.gz
reads:
- _R1
- _R2
gendiag: .test/fake_gendiag.yaml
memory:
  star: 8000
  bowtie2: 1000
  salmon: 8000
  fastqc: 1000
  multiqc: 1000
cpu:
  star: 2
  bowtie2: 1
  salmon: 8
  fastqc: 8
reference : human
transcripts: .test/data/ressources/transcriptome/transcripts.fa
index:
  star: .test/data/ressources/genome_index
  bowtie2: .test/data/ressources/bowtie2_index/goldenSnidget
```

Tanuki indeed comes with its own test data and can therefore be tested before using your own data. This config file has adjusted ressources to run on a small dataset. See "Ressources and running time" to learn more about the "real dataset" ressources.

> **_NOTE:_** /!\ The output directory must exists !


## Options

You can use four optionnal arguments :

-If your reads pairing information does not following the default representation : ```_R1 _R2``` :

```bash
python init_workflow.py -i "/path/to/my/raw/reads" -o "/path/to/store/my/results" --pairing "_1" "_2" 
```

- If your reads are in an other format than .fastq.gz : 

```bash
python init_workflow.py -i "/path/to/my/raw/reads" -o "/path/to/store/my/results" --extension ".fq.gz" 
```

- If your reads are unpaired : 

```bash
python init_workflow.py -i "/path/to/my/raw/reads" -o "/path/to/store/my/results" --unpaired
```

- If you need an other organism that the default (human), (Note : Only human and mouse are available for now, more can be requested) :

```bash
python init_workflow.py -i "/path/to/my/raw/reads" -o "/path/to/store/my/results" -r mouse
```

Optional argument to come : 
- Salmon only aproach to skip star
- Create archive for 3DRNAseq


## Output files

In the output folder given as parameter, tanuki will structure its outputs in the following way :

```bash
.
├── bowtie2
├── fastqc
├── multiqc
├── salmon
│   ├── 2048_HV_L002
│   ├── 2048_HV_L004
│   ├── 302_Day7_L003
│   ├── 905_D1pi1_L004
│   ├── 905_D1pi1_L005
│   └── 939_Day1_L004
└── star
    ├── 2048_HV_L002
    ├── 2048_HV_L004
    ├── 302_Day7_L003
    ├── 905_D1pi1_L004
    ├── 905_D1pi1_L005
    └── 939_Day1_L004

```

In each directory, here are the output you will find : 

- **Bowtie2**

 Bowtie2 outputs contains just the log of the alignement so they can be displayed by multiqc. No alignment files are kept as it is only a quality control and we want to save storing space.

 There will be a log file per sample in the bowtie2 folder.

- **FastQC**

Fastqc folder contains html files and the zip files from fastqc. That's it :)

- **Multiqc**

Mulitqc folder contains the final report regrouping output from all the other tools.

- **Salmon**

In the salmon directory, each sample will have its own subdirectory. Containing itself salmon outputs.
The quantification information itself is contained in the files named quant.sf.

For information, the transcripts given to salmon were obtained with our Hg19 reference on the cluster + the gtf files of their transcripts using gffread with this command : 

```bash
gffread -w Homo_sapiens.GRCh38.gffread.transcripts.fa -g /net/beegfs/hgn/pub/02.References/01.Genomes/02.Ensembl/01.Homo_Sapiens/Homo_sapiens.GRCh38.dna.primary_assembly.fa transcriptome/Homo_sapiens.GRCh38.103.gtf
```

- **STAR**

In the star directory, same as for sample, there is a subdirectory per sample, and each subdirectory contains the star outputs.
The bam file you can find here was generated specially for salmon to be in the transcripts coordinates.

For information, the star index used for the alignement have been generated with the following parameters : 

```bash
STAR --runThreadN 6 --runMode genomeGenerate --genomeDir ./index --genomeFastaFiles /net/beegfs/hgn/pub/02.References/01.Genomes/02.Ensembl/01.Homo_Sapiens/Homo_sapiens.GRCh38.dna.primary_assembly.fa --sjdbGTFfile transcriptome/Homo_sapiens.GRCh38.103.gtf --sjdbOverhang 99 --genomeSAindexNbases 7
```

## Logs

- snakemake logs can be found in the .snakemake/log folder
- log for each rule can be found in tanuki/logs/theRule


## Ressources and running time

For now, the ressources for every tool are fixed by us in he config file. If needed you can always edit it manually of course, but it is not advised to do so.

Here are the ressources tanuki attribute to each tool : 

memory:
  star: 32000
  bowtie2: 2000
  salmon: 10000
  fastqc: 2000
  multiqc: 2000

cpu:
  star: 10
  bowtie2: 4
  salmon: 8
  fastqc: 8




### Working notes

<details>
  <summary markdown="span">Secret hidden area where I keep some useful notes for myself </summary>
Oh no ! I was busted !

#### Fixing the mock test


```
agat_convert_sp_gxf2gxf.pl --gff ressources/annotation/features.gff > fixed-features.gff
```

```
gffread -v fixed-features.gff -T -o fixed-features.gtf
```

```
gffread -w new-transcripts.fa -g ressources/genome/genome.fa ressources/annotation/fixed-features.gtf
```

#### STAR

##### Indexation :


```
STAR --runThreadN 6 --runMode genomeGenerate --genomeDir ressources/genome_index --genomeFastaFiles ressources/genome/genome.fa --sjdbGTFfile ressources/annotation/fixed-features.gtf --sjdbOverhang 99 --genomeSAindexNbases 7
```


##### Alignement
```
STAR --genomeDir ressources/genome_index --runThreadN 2 --readFilesIn <(gunzip -c rawdata/BORED_2_R1.fq.gz) <(gunzip -c rawdata/BORED_2_R2.fq.gz) --outFileNamePrefix test_gtf --outSAMtype BAM Unsorted --outStd BAM_Unsorted --outSAMunmapped Within --quantMode TranscriptomeSAM | samtools sort -m '2G' -T temp_dir/test -@ 5 -O bam -o out-gff.bam - 2> test_log.txt
```
 
</details>

